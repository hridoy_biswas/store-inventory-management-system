<?php

namespace App\Http\Controllers;

use App\Models\ProductSizeStock;
use App\Models\ReturnProduct;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ReturnProductsController extends Controller
{
    public function returnProduct()
    {
        return view('return_products.return');
    }

    public function returnProductSubmit(Request $request)
    {
       ////Validation////
       $validate = Validator::make($request->all(), [
        'product_id' => 'required|numeric',
        'date' => 'required|string',
        'items' => 'required'

    ]);
    /////ERROR///////
    if($validate->fails()){
        return response()->json([
            'success' => false,
            'errors' => $validate->errors()
        ],Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    ////Store Product stock//
    // return $request->all();
    foreach($request->items as $item){
        if($item['quantity'] && $item['quantity'] > 0){
            $data = new ReturnProduct();
            $data->product_id = $request->product_id;
            $data->date = $request->date;
            $data->size_id = $item['size_id'];
            $data->quantity = $item['quantity'];
            $data->save();


            ////product stock size update////
            $psq = ProductSizeStock::where('product_id', $request->product_id)
            ->where('size_id', $item['size_id'])
            ->first();

            //stock in
            $psq->quantity = $psq->quantity + $item['quantity'];

            $psq->save();
        }
        
    }
        flash('Product Return Updated Successfully')->success();
        return response()->json([
            'success' => true
        ],Response::HTTP_OK);

    }

    public function history(){
        $return_products = ReturnProduct::with(['product', 'size'])->orderBy('created_at', 'DESC')->get();
        return view('return_products.history', compact('return_products'));
    }
}
