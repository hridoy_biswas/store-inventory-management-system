<?php

namespace App\Http\Controllers;

use App\Models\ProductSizeStock;
use App\Models\ProductStock;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class StocksController extends Controller
{
    public function stock()
    {
        return view('stocks.stock');
    }

    public function stockSubmit(Request $request)
    {
       ////Validation////
       $validate = Validator::make($request->all(), [
        'product_id' => 'required|numeric',
        'date' => 'required|string',
        'stock_type' => 'required|string',
        'items' => 'required'

    ]);
    /////ERROR///////
    if($validate->fails()){
        return response()->json([
            'success' => false,
            'errors' => $validate->errors()
        ],Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    ////Store Product stock//
    // return $request->all();
    foreach($request->items as $item){
        if($item['quantity'] && $item['quantity'] > 0){
            $data = new ProductStock();
            $data->product_id = $request->product_id;
            $data->date = $request->date;
            $data->status = $request->stock_type;
            $data->size_id = $item['size_id'];
            $data->quantity = $item['quantity'];
            $data->save();


            ////product stock size update////
            $psq = ProductSizeStock::where('product_id', $request->product_id)
            ->where('size_id', $item['size_id'])
            ->first();

            if($request->stock_type == ProductStock::STOCK_IN){
                $psq->quantity = $psq->quantity + $item['quantity'];
            }else{
                $psq->quantity = $psq->quantity - $item['quantity'];
            }

            $psq->save();
        }
        
    }
        flash('Stock Updated Successfully')->success();
        return response()->json([
            'success' => true
        ],Response::HTTP_OK);

    }

    public function history(){
        $stocks = ProductStock::with(['product', 'size'])->orderBy('created_at', 'DESC')->get();
        return view('stocks.history', compact('stocks'));
    }
}
