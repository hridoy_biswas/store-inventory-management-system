import Vue from 'vue'

import Vuex from 'vuex'

// Vue.use(Vuex)

////Modules///
import errors from './modules/utils/errors/index.js'
import categories from './modules/categories/index.js'
import brands from './modules/brands/index.js'
import sizes from './modules/sizes/index.js'
import products from './modules/products/index'
import stocks from './modules/stocks/index'
import return_products from './modules/return_products/index'


export default new Vuex.Store({
    strict: false,
    modules: {
        errors,
        categories,
        brands,
        sizes,
        products,
        stocks,
        return_products
    }
})
