// //ERRORS
// export const GET_ERRORS = 'GET_ERRORS'

//Categories

export const GET_CATEGORIES = 'GET_CATEGORIES'

//BRANDS
export const GET_BRANDS = 'GET_BRANDS'

//SIZES
export const GET_SIZES = 'GET_BRANDS'

//PRODUCT
export const ADD_PRODUCT = 'ADD_PRODUCT'
export const EDIT_PRODUCT = 'EDIT_PRODUCT'
export const GET_PRODUCTS = 'GET_PRODUCTS'

//STOCK
export const SUBMIT_STOCK = 'SUBMIT_STOCK'

//Return Product
export const SUBMIT_RETURN_PRODUCT = 'SUBMIT_RETURN_PRODUCT'