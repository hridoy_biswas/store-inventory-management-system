// import './bootstrap';

// import Alpine from 'alpinejs';

// window.Alpine = Alpine;

// Alpine.start();

// require('./bootstrap');

// window.Vue = require('vue');

// Vue.component('example-components', require('./components/ExampleComponents.vue').default);

// const app = new Vue({
//     el:'#app'
// })

require ('./bootstrap');

import { createApp } from 'vue'
import store from './store'
import ExampleComponents from './components/ExampleComponents'
import ProductAdd  from './components/Products/ProductAdd'
import ProductEdit  from './components/Products/ProductEdit'
import StockManage from './components/stocks/StockManage'
import ReturnProduct from './components/return_products/ReturnProduct'
import Select2 from 'vue3-select2-component';


const app = createApp({})
app.component('example-component', ExampleComponents)
app.component('product-add', ProductAdd)
app.component('product-edit', ProductEdit)
app.component('stock-manage', StockManage)
app.component('return-product', ReturnProduct)
app.use(store)
app.component('Select2', Select2)
app.mount('#app')