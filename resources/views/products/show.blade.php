@extends('layouts.master')

@section('content')
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Products Show</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
            <li class="breadcrumb-item active">Product Show</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

  <div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <div class="card card-primary card-outline">
                    <div class="card-body">
                      <h5 class="card-title">Product Info</h5><br>
                        <div class="card-body">
                         <table class="table table-sm table-bordered"> 
                            <tr>
                                <td>Name</td>
                                <td>{{ $product->name ?? '' }}</td>
                            </tr>
                            <tr>
                                <td>Category</td>
                                <td>{{ $product->category->name ?? '' }}</td>
                            </tr>
                            <tr>
                                <td>Brand</td>
                                <td>{{ $product->brand->name ?? '' }}</td>
                            </tr>
                            <tr>
                                <td>SKU</td>
                                <td>{{ $product->sku ?? '' }}</td>
                            </tr>
                            <tr>
                                <td>Cost Price</td>
                                <td>{{ $product->cost_price ?? '' }}</td>
                            </tr>
                            <tr>
                                <td>Retail Price</td>
                                <td>{{ $product->retail_price ?? '' }}</td>
                            </tr>
                            <tr>
                                <td>Year</td>
                                <td>{{ $product->year ?? '' }}</td>
                            </tr>
                            <tr>
                                <td>Description</td>
                                <td>{{ $product->description ?? '' }}</td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>{{ $product->status ? 'Active' : 'InActive' }}</td>
                            </tr>
                         </table>
                        </div>
                        <div class="card-footer"> 
                            <a class="btn btn-sm btn-dark" href="{{ route('products.index') }}"><i class="fa fa-arrow-left"></i>Back</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card card-primary card-outline">
                    <div class="card-body">
                      <h5 class="card-title">Product Image</h5><br>
                        <div class="card-body text-center">
                            <img style="width: 300px; height:200px" src="{{ asset('storage/product_images/' . $product->image) }}">
                        </div>
                    </div>
                </div>
                <div class="card card-primary card-outline">
                    <div class="card-body">
                      <h5 class="card-title">Product Stock</h5><br>
                        <div class="card-body">
                            <table class="table table-sm table-bordered">
                                <thead>
                                    <tr>
                                        <th>Size</th>
                                        <th>Location</th>
                                        <th>Quantity</th>
                                    </tr>
                                </thead>
                                @foreach ($product->product_stocks as $p_stock)
                                <tbody>
                                    <tr>
                                        <th>{{ $p_stock->size->size ?? '' }}</th>
                                        <th>{{ $p_stock->location ?? '' }}</th>
                                        <th>{{ $p_stock->quantity ?? '' }}</th>
                                    </tr>
                                </tbody>
                                @endforeach 
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
@endsection